\documentclass[a4paper,12pt]{article}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{subfigure}
\usepackage{subfigmat}
\usepackage{multirow}
\usepackage{amsmath,soul}

\addtolength{\hoffset}{-1.5cm}
\addtolength{\textwidth}{3.5cm}

\title{\underline{\textbf{\Large{Answers \& Rebuttals for Reviewer \#2's Comments}}}}
\date{}

\begin{document}
\maketitle

  First of all, we would like to appreciate your careful review and valuable comments. In response to the comments by you and by other reviewer, we made a substantial revision. For your specific comments, we revised our paper as follows:
  \newline\newline
  \textbf{Comments to the Author:}
  \newline
  In summary the manuscripts presents the development of computational components to overcome difficulties arising from these issues. The modification of the shock-discontinuity-sensing term used in the two-phase AUSMPW+ and RoeM schemes was done, as this term is suitable for complex equation of state of real fluids. It is demonstrated that the resulting schemes are capable of robustly and accurately capturing phenomena involving phase and shock front and two-phase interface. The numerical accuracy also is significantly improved.
  \newline\newline
  \textbf{General Comments:}
  \newline
  Some general comments are presented here. Detailed comments on particular sections are also provided.
  \newline
  The numerical methods are of interest for the community. However, \ul{further simulations are needed} to make the conclusions more convincing. It is not very convincing that the present accuracy is improved significantly because there is not the \ul{verification of grid resolution}. The numerical results also need to \ul{compare with the previous simulations or experiments}.
  \newline\newline
  \textbf{Comment 1.} Authors say that the present schemes are operating successfully in controlling the amount of numerical dissipation near the liquid phase, but they do not \ul{compare quantitatively the present schemes with other schemes such as GFM and RGFM}.
  \newline\newline
  \textbf{Answer for Comment 1:}
  \newline
  As known well, there are many approaches for the numerical/physical modeling of multi-phase flow phenomena. Each approach has its own advantages and limitations, and has been developed with the purpose of covering specific multi-phase flow phenomena. As a result, it would be difficult to make apple-to-apple comparison to assert that a certain model/approach prevails over the others for a wide range of multi-phase flows, from the view point of numerical accuracy, robustness and computational efficiency. Basically, numerical approaches for multi-phase computations can be divided into two categories from the view point of treating phase interface: interface tracking method and interface capturing method. The ghost fluid method (GFM, Ref. [1.1] below) and real ghost fluid method (rGFM, Ref. [1.2] below) belong to the interface tracking method which tracks an initially given phase interface by employing the level set method. In this class of methods, the phase interface is considered as a moving boundary, and the flow information is exchanged between two (or more) systems of equations in the form of boundary conditions. This approach is non-conservative, but the phase tracking method can sharply determine the interface location due to the absence of numerical diffusion across the interface. Thus, it is advantageous for multi-phase problems with a distinct phase interface, though there is some recent progress to describe a phase transition by modifying Tait's equation of state (EOS) (For example, Ref. [1.3] below).
  
  On the other hand, the present approach belongs to the interface capturing method at which numerical methods developed for single phase flows are extended by introducing additional numerical components. In this class of methods, numerical diffusion across physical discontinuities, such as shocks, contact discontinuity and phase interface, is inevitable. From the view point of sharp interface capturing, numerical diffusion can be considered as a kind of  impediment, but it cannot be eliminated completely since it is essential for capturing shock/contact discontinuities without unwanted oscillations. The present method with homogeneous mixture model may yield a less-sharp phase interface (but not shock/contact discontinuities) compared to the phase tracking method. However, it is simple and satisfies conservation, and numerical schemes developed for single phase flows can be efficiently extended into homogeneous mixture model without extra treatment for conservation issue. 
  
  More importantly, one of the obectives in this work is to provide computational methods which can predict the cryogenic flows inside aerospace launch vehicles. In this case, \ul{incompressible and compressible flow fields completely co-exist while accompanying shocks and phase-transition. On top of it, a mathematical/analytic form of EOS is not available at all. The proposed approach is quite advantageous to handle this type of flows}. Overall, the proposed numerical methods are designed to compute all-speed real fluid flows independent of type of EOS, including shocks, phase change, cavitation, and gas-liquid interaction within the framework of (2nd-to-3rd)th-order finite volume discretization.
  Within this context, the present work deals with compressible two-phase problems (subsections A, B in section V of the revised manuscript), phase-changing problems with various EOS (subsections F--H in section V of the revised manuscript), and all-speed problems (subsections C--H in section V of the revised manuscript). Throughout the computations, we have focused on the versatility/accuracy/efficiency of the proposed methods for a wide range of multi-phase applications, although some of them can be reproduced by an interface tracking method.

  \begin{figure}[hbt!]
  	\renewcommand\thefigure{1.1}
  	\begin{subfigmatrix}{3}
  		\subfigure[Density]{\includegraphics{figure/GFMdensity.png}}
  		\subfigure[Velocity]{\includegraphics{figure/GFMvelocity.png}}
  		\subfigure[Pressure]{\includegraphics{figure/GFMpressure.png}}
  	\end{subfigmatrix}
  	\caption{Problem 3 of Ref. [1.2]: results of GFM [1.2]}
  	\label{fig:problem3_GFM}
  \end{figure}
  \begin{figure}[hbt!]
  	\renewcommand\thefigure{1.2}
  	\begin{subfigmatrix}{3}
  		\subfigure[Density]{\includegraphics{figure/rGFMdensity.png}}
  		\subfigure[Velocity]{\includegraphics{figure/rGFMvelocity.png}}
  		\subfigure[Pressure]{\includegraphics{figure/rGFMpressure.png}}
  	\end{subfigmatrix}
  	\caption{Problem 3 of Ref. [1.2]: results of rGFM [1.2]}
  	\label{fig:problem3_rGFM}
  \end{figure}   
  \begin{figure}[hbt!]
  	\renewcommand\thefigure{1.3}
  	\begin{subfigmatrix}{3}
  		\subfigure[Density]{\includegraphics{figure/density}}
  		\subfigure[Velocity]{\includegraphics{figure/velocity}}
  		\subfigure[Pressure]{\includegraphics{figure/pressure}}
  	\end{subfigmatrix}
  	\caption{Problem 3 of Ref. [1.2]: results of AUSMPW+\_N and RoeM\_N}
  	\label{fig:problem3_present}
  \end{figure}

  In order for computational comparison (mainly, accuracy) with GFM/rGFM, we compute `Problem 3: Gaseous shock in impedance-matching medium' described in Ref. [1.2] below with the same computational conditions. Figures \ref{fig:problem3_GFM}, \ref{fig:problem3_rGFM} and \ref{fig:problem3_present} show that the proposed methods capture shock discontinuity very well compared with GFM and rGFM results, but the fluid interface ($x=0.76$) is more diffusive than GFM/rGFM, due to the reason mentioned previously. We feel sorry that these results could not be included in the revised manuscript because we were requested by other reviewer to reduce the substantially oversized original manuscript to meet with the AIAA standard format. 

  Some of above contents with Refs. [1.1--1.3] below are added in the revised manuscript (page 2 line 23 -- page 3 line 5).
  \newline\newline
  {\small\textbf{Reference for Comment 1:}}
  \newline
  [1.1] Fedkiw, R. P., Aslam, T., Merriman, B., and Osher, S., ``A non-oscillatory Eulerian approach to interfaces in multimaterial flows (the ghost fluid method)," {\it Journal of computational physics}, Vol. 152, No. 2, 1999, pp. 457--492. doi:10.1006/jcph.1999.6236.\newline
  [1.2] Wang, C. W., Liu, T. G., and Khoo, B. C., ``A Real Ghost Fluid Method for the Simulation of Multimedium Compressible Flow," {\it SIAM Journal on Scientific Computing}, Vol. 28, No. 1, July 2006, pp. 278--302. doi:10.1137/030601363. \newline
  [1.3] Houim, R. W., and Kuo, K. K., ``A ghost fluid method for compressible reacting flows with phase change," {\it Journal of Computational Physics}, Vol. 235, No. Supplement C, 2013, pp. 865--900. doi:https://doi.org/10.1016/j.jcp.2012.09.022.
  \newline\newline\newline
  \textbf{Comment 2.} For the simulations of water-to-air shock-tube problems, how was \ul{the interface between the gas and liquid phase tracked}?
  \newline\newline
  \textbf{Answer for Comment 2:}
  \newline
  As stated in `Answer for Comment 1', the present numerical methods are based on the interface capturing concept. In other words, we design a cell-interface numerical flux (or AUSMPW+\_N, RoeM\_N schemes) within the framework of FVM (Finite Volume Method) discretization, and all discontinuities including shocks and phase interface are implicitly recognized/captured by computing the mass fraction as a dependent variable of the governing equations (see Eqs. (2), (4), and (5)). Thus, we do not track the phase interface by employing a level set equation.
  \newline\newline\newline
  \textbf{Comment 3.} In this manuscript, the author first described the original SDST and SDST in reference [15] in more details, and then given the modified shock-discontinuity-sensing term (SDST) used in the two-phase schemes. However, the author only compared those methods through 1D normal shock relation in Fig. 1. The reviewer suggests authors add more examples to further \ul{demonstrate the advantages of the modified method besides one-dimensional problems}.
  \newline\newline
  \textbf{Answer for Comment 3:}
  \newline
  The 2-D shock/water-column interaction problem in subsection B of section V is considered to examine the behavior of computed results depending on the choice of a shock-discontinuity-sensing term ($\Pi^{o}_{1/2}$ in Eq. (19), $\Pi_{1/2}$ in Eq. (20), $\Pi^{*}_{1/2}$ in Eq. (26)). As an initial condition, pure gas with $\epsilon_1=1$ and pure liquid with $\epsilon_2=0$ are imposed for more severe test.
  
  \begin{figure}[hbt!]
    \renewcommand\thefigure{3.1}
    \begin{subfigmatrix}{3}
      \subfigure[$1/\Pi^{o}_{1/2}$\label{fig:ori_ini}]{\includegraphics{figure/ini_ori.png}}
      \subfigure[$1/\Pi_{1/2}$\label{fig:prv_ini}]{\includegraphics{figure/ini_prv.png}}
      \subfigure[$1/\Pi^{*}_{1/2}$\label{fig:new_ini}]{\includegraphics{figure/ini_new.png}}
    \end{subfigmatrix}
    \caption{Inverse values of shock-discontinuity-sensing terms (upper half) and numerical Schlieren images (lower half) of shock/water-column interaction problem with $\epsilon_1=1,\epsilon_2=0$ at $t=0\ \mathrm{\mu s}$.}
    \label{fig:Inverse SDST at t=0}
  \end{figure} 
  As seen in Fig. \ref{fig:Inverse SDST at t=0}, three SDST terms successfully recognize the initial moving shock by showing similar values with one another. As computations proceed, however, $\Pi^{o}_{1/2}$ and $\Pi_{1/2}$ grossly misinterpret non-shock region (the phase interface) as shock region (Figs. 3.2(a) and 3.2(b)), while $\Pi^{*}_{1/2}$ successfully recognizes the shock-discontinuity only (Fig. 3.2(c)). The inverse values of $\Pi^{o}_{1/2}$ and $\Pi_{1/2}$ near the phase interface are unacceptably high; they are mostly over 5 and some of them are even higher than 1000. This means that $\Pi^{o}_{1/2}$ and $\Pi_{1/2}$ are sensing the phase interface as a discontinuity much stronger than the initial shock discontinuity. As a result, numerical flux (Eq. (45) for RoeM\_N and Eq. (51) for AUSMPW+\_N) with $\Pi^{o}_{1/2}$ or $\Pi_{1/2}$ will provide grossly inaccurate numerical dissipation across the phase interface triggering numerical instability. This is particularly aggrevated when there exist shock-shock or shock-phase interface interactions. Indeed, computations with $\Pi^{o}_{1/2}$ or $\Pi_{1/2}$ erroneously capture the phase interface (Figs. 3.3(a) and 3.3(b)) and eventually fail at about $t = 9\ \mu s $. On the contrary, the new shock-discontinuity-sensing term successfully senses the two-phase shock discontinuity without confusing it with the circular phase interface, and provides well-scaled numerical dissipation to capture the relevant flow physics.
  
  Above contents are added in section V (subsction B) of the revised manuscript (page 19 line 9 -- page 20 line 4).
  \begin{figure}
  	\renewcommand\thefigure{3.2}
  	\begin{subfigmatrix}{2}
  		\subfigure[$1/\Pi^{o}_{1/2}$\label{fig:ori}]{\includegraphics{figure/ori}}
  		\subfigure[$1/\Pi_{1/2}$\label{fig:prv}]{\includegraphics{figure/prv}}
  		\subfigure[$1/\Pi^{*}_{1/2}$\label{fig:new}]{\includegraphics{figure/new}}
  	\end{subfigmatrix}
  	\caption{Inverse values of shock-discontinuity-sensing terms (upper half) and numerical Schlieren images (lower half) of shock/water-column interaction problem with $\epsilon_1=1,\epsilon_2=0$ at $t=8.0\ \mathrm{\mu s}$.}
  	\label{fig:Inverse SDST at t=8.0}
  \end{figure}
  \begin{figure}[ht!]
  	\renewcommand\thefigure{3.3}
  	\begin{subfigmatrix}{3}
  		\subfigure[$1/\Pi^{o}_{1/2}$\label{fig:alp_ori}]{\includegraphics{figure/alp_ori.png}}
  		\subfigure[$1/\Pi_{1/2}$\label{fig:alp_prv}]{\includegraphics{figure/alp_prv.png}}
  		\subfigure[$1/\Pi^{*}_{1/2}$\label{fig:alp_new}]{\includegraphics{figure/alp_new.png}}
  	\end{subfigmatrix}
  	\caption{Volume fraction of gas phase for shock/water-column interaction problem with $\epsilon_1=1,\epsilon_2=0$ at $t=8.0\ \mathrm{\mu s}$.}
  	\label{fig:Volume fraction of gas phase at t=8.0}
  \end{figure}
  \newline\newline\newline
  \textbf{Comment 4.} The highlight is that the accuracy on the AUSMPW+ and RoeM schemes has been improved. Authors also adopted several numerical examples to compare the accuracy of each scheme by curve plots. But it is difficult for readers to realize \ul{how much order the two improved schemes can be reached}. The more convincing method adopted in ``Cheng Wang, Chi-Wang Shu, Wenhu Han, et al. High resolution WENO simulation of 3D detonation Waves [J]. Combustion and Flame, 2013, 16:447-462.'' for validating the accuracy of each scheme, which is also a common and typical way used in the computational fields.
  \newline\newline
  \textbf{Answer for Comment 4:}
  \newline
  In general, FVM-based schemes are composed of two essential components: i) a cell-interface numerical flux which is 1st-order accurate, and ii) a higher-order monotonic interpolation which provides a higher-order flux value starting from the cell-interface numerical flux. For example, in cases of WENO schemes, LLF(local Lax-Friedrich) flux is typically used as a cell-interface flux, and then WENO interpolation procedure is introduced to obtain a higher-order flux value. \ul{The purpose of the present work is to design a cell-interface flux (RoeM\_N or AUSMPW+\_N scheme) which can handle all-speed real fluid flows, including shocks, phase change, cavitation, and gas-liquid interaction, irrespective of type of EOS. Once the cell-interface numerical flux is developed, a higher-order flux value can be readily obtained by combining the RoeM\_N and/or AUSMPW+\_N schemes with any higher-order monotonic interpolation, such as, TVD-MUSCL, ENO/WENO or MLP}. In the present work, RoeM\_N and AUSMPW+\_N schemes are combined with FVM-based MLP monotonic interpolation scheme which has been developed by authors. Detailed numerical analyses/characteristics of FVM-based MLP including results of grid refinement study can be found in Ref. [4-2] below.
  
  Nonetheless, a grid refinement test is carried out using the unsteady inviscid vortex propagation problem in subsection D of section V to verify that the proposed fluxes are fully compatible with a higher-order interpolation scheme. MLP5 and MUSCL with van Leer limiter are used in the present work. Table \ref{Table:Grid refinement study for unsteady inviscid vortex propagation problem} shows $L\textsuperscript{$\infty$, 1, 2}$ errors at $t=0.2304\ \mathrm{s}$. The order of accuracy with MLP5 is clearly better than that with conventional MUSCL-type limiter (van Leer limiter).
  
  In addition, the benefit of the proposed numerical flux needs to be examined in terms of convergence characteristics as well as accuracy. Table 4.2 below shows the improvement in accuracy particularly in computations with acoustic time scale. The substantial improvement in convergence can be seen in Figs. 8(b), 8(d) and 9(b), 9(d) in the revised manuscript. In other words, the RoeM\_N and AUSMPW+\_N schemes retain the advatages of both unsteady preconditioning (in terms of convergence) and steady preconditioning (in terms of accuracy) simultaneously.

  We feel sorry again that the results of above grid refinement test could not be included in the revised manuscript, because we were requested by other reviewer to reduce the volume of the original manuscript to meet with the AIAA standard format. 
  \begin{table}[hbt!]
  	\renewcommand\thetable{4.1}
  	\caption{Grid refinement study for unsteady inviscid vortex propagation problem (CFL\textsubscript{c}=1)}
  	\label{Table:Grid refinement study for unsteady inviscid vortex propagation problem}
  	\centering
  	\resizebox{\textwidth}{!}{
  		\begin{tabular}{lllllllll}
  			\hline
  			\multicolumn{2}{l}{Scheme} & Size & $L\textsuperscript{$\infty$}$ error & $L\textsuperscript{$\infty$}$ order & $L\textsuperscript{1}$ error & $L\textsuperscript{1}$ order & $L\textsuperscript{2}$ error & $L\textsuperscript{2}$ order\\ \hline
  			\multirow{12}{*}{MLP5} & \multirow{6}{*}{RoeM\_N} & 40x40 & 2.66194E-01 & -- & 9.60939E-04  & -- & 9.11799E-03 & -- \\
  			& & 60x60 & 9.15271E-02 & 2.633 & 3.10789E-04 & 2.784 & 2.95736E-03 & 2.777 \\
  			& & 80x80 & 4.52902E-02 & 2.446 & 1.59607E-04 & 2.316 & 1.47786E-03 & 2.411 \\
  			& & 160x160 & 1.01892E-02 & 2.152 & 3.79504E-05 & 2.072 & 3.40193E-04 & 2.119 \\
  			& & 240x240 & 4.48075E-03 & 2.026 & 1.68142E-05 & 2.008 & 1.49955E-04 & 2.020 \\
  			& & 320x320 & 2.51430E-03 & 2.008 & 9.46392E-06 & 1.998 & 8.41737E-05 & 2.007 \\
  			\cline{2-9}
  			& \multirow{6}{*}{AUSMPW+\_N} & 40x40 & 2.56299E-01 & -- & 9.41225E-04  & -- & 8.90547E-03 & -- \\
  			& & 60x60 & 8.96437E-02 & 2.591 & 3.08088E-04 & 2.754 & 2.92156E-03 & 2.749 \\
  			& & 80x80 & 4.47916E-02 & 2.517 & 1.59022E-04 & 2.565 & 1.46874E-03 & 2.600 \\
  			& & 160x160 & 1.01720E-02 & 2.139 & 3.79298E-05 & 2.068 & 3.39893E-04 & 2.111 \\
  			& & 240x240 & 4.47839E-03 & 2.023 & 1.68135E-05 & 2.006 & 1.49916E-04 & 2.019 \\
  			& & 320x320 & 2.51364E-03 & 2.008 & 9.46507E-06 & 1.997 & 8.41643E-05 & 2.007 \\
  			\hline
  			& \multirow{6}{*}{RoeM\_N} & 40x40 & 1.53108 & -- & 6.37113E-03  & -- & 5.43754E-02 & -- \\
  			& & 60x60 & 1.04045 & 0.953 & 3.79366E-03 & 1.279 & 3.30112E-02 & 1.231 \\
  			& & 80x80 & 8.25067E-01 & 0.806 & 2.68276E-03 & 1.204 & 2.28747E-02 & 1.275 \\
  			\multirow{2}{*}{MUSCL}& & 160x160 & 3.71555E-01 & 1.151 & 1.30844E-03 & 1.036 & 1.16592E-02 & 0.972 \\
  			& & 240x240 & 3.12789E-01 & 0.425 & 7.85670E-04 & 1.258 & 8.01712E-03 & 0.924 \\
  			{(van Leer} & & 320x320 & 2.59503E-01 & 0.649 & 5.40808E-04 & 1.298 & 5.96475E-03 & 1.028 \\
  			\cline{2-9}
  			\multirow{2}{*}{~limiter)} & \multirow{6}{*}{AUSMPW+\_N} & 40x40 & 1.39557 & -- & 5.87291E-03  & -- & 5.96475E-03 & -- \\
  			& & 60x60 & 9.06045E-01 & 1.065 & 3.61385E-03 & 1.198 & 3.06856E-02 & 1.240 \\
  			& & 80x80 & 7.13510E-01 & 0.830 & 2.56681E-03 & 1.189 & 2.13809E-02 & 1.256 \\
  			& & 160x160 & 3.47604E-01 & 1.037 & 1.27462E-03 & 1.010 & 1.12075E-02 & 0.932 \\
  			& & 240x240 & 2.99895E-01 & 0.364 & 7.76249E-04 & 1.223 & 7.80365E-03 & 0.893 \\
  			& & 320x320 & 2.53189E-01 & 0.588 & 5.37195E-04& 1.280 & 5.84713E-03 & 1.003 \\
  			\hline
  		\end{tabular}
  	}
  \end{table}
  \begin{table}[hbt!]
  	\renewcommand\thetable{4.2}
  	\caption{Comparison of vorticity at vortex center}
  	\label{Table:The vorticities at vortex center and errors}
  	\centering
  	\resizebox{\textwidth}{!}{
  		\begin{tabular}{lccccc}
  			\hline
  			& Exact  & \multirow{2}{*}{AUSMPW+\_N} & AUSMPW+ with & \multirow{2}{*}{RoeM\_N} & RoeM with \\
  			& Sol.  &  & pure unsteady precon. &  & pure unsteady precon.\\
  			\hline
  			\multirow{2}{*}{CFL\textsubscript{u}=1} & 2.89344  & 2.48311 & 2.47510 & 2.48335 & 2.46854\\
  			\cline{2-6}
  			& error  & -14.182 \% & -14.458 \% & -14.173 \% & -14.685 \%\\
  			\hline
  			\multirow{2}{*}{CFL\textsubscript{c}=1} & 2.89344  & 2.80782 & 2.63525 & 2.80841 & 2.64485\\
  			\cline{2-6}
  			& error  & -2.959 \% & -8.923 \% & -2.939 \% & -8.592 \%\\
  			\hline
  		\end{tabular}
  }
  \end{table}
  \newline\newline
  {\small\textbf{Reference for Comment 4:}}
  \newline
  [4.1] Wang, C., Shu, C. W., Han, W., and Ning, J., ``High resolution WENO simulation of 3D detonation waves," {\it Combustion and Flame}, Vol. 160, No. 2, 2013, pp. 447--462. doi: 10.1016/j.combustflame.2012.10.002
  \newline
  [4.2] Yoon, S.-H., Kim, C., and Kim, K. H., ``Multi-dimensional limiting process for three-dimensional flow physics analyses," {\it Journal of Computational Physics}, Vol. 227, No. 12, 2008, pp. 6001--6043. doi: 10.1016/j.jcp.2008.02.012.
  \newline\newline\newline
  \textbf{Comment 5.} The Authors only given the numerical examples to validate the efficiency and accuracy of the new method, however, the reviewer would like to see \ul{more examples comparing with the published results of simulations or experiments} for demonstrating the so-called ``accurately capturing the flow fields".
  \newline\newline
  \textbf{Answer for Comment 5:}
  \newline
  We do want to add various multi-phase examples but, due to the AIAA constraint on the manuscript volume and the request of other reviewer to shorten the original manuscript, we just add one more computational results on steam condensing flows compared with experimental data. This validation problem demonstrates that the proposed numerical methods are able to capture two-phase condensing flow fields with sudden phase change leading to so-called condensation shock. The validation case is added in subsection F in section V of the revised manuscript.
  \newline\newline
\end{document}
